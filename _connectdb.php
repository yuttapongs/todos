<?php 
function getDbConn(){
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "todomap";
    $conn = "" ; 
    try {
        $conn = new PDO("mysql:host=$servername;dbname=" . $dbname , $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "<div class='text-primary'> Connected successfully</div>";
        }
    catch(PDOException $e)
        {
        echo "Connection failed: " . $e->getMessage();
        }   
    $conn->query("SET NAMES utf8");
    return $conn;
}

$conn = getDbConn();

?>
