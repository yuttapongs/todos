
<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


    <div class="container">
    <?php 
include("_connectdb.php");
$sql = " SELECT id, jsdata FROM todos 
          WHERE ( jsdata like  '%\"status\":\"unfinish\"%' ) 
              OR ( jsdata like  '%\"status\":\"\"%' ) 
              OR ( jsdata like  '%\"status\":\"finished\"%' ) 
              OR ( jsdata like  '%\"status\":\"doing\"%' ) 
          ORDER BY id " ;
?>
<?php 
    include("_menu.php");
    include("_info.php");
    // echo " <br>";
    // echo $sql ;
    // echo " <br>"; 


    $ar_tr = ["finished"=> "<b class='text-success'>finished</b>" , 
                "unfinish"=> "<b class='text-danger'>unfinish</b>" ,   
                "doing"=> "<b class='bg-info'>doing</b>" ,
                "bin"=> "<b class='bg-info'>bin</b>"    
                ];

 foreach ($conn->query($sql) as $row) {
    $jsdata = json_decode($row['jsdata']);
    if($jsdata->status=='bin') continue; // go to foreach again
    echo "<br>";
    print $row['id'] . "\t";
    print $row['jsdata'] . "\t";
    // print_r($jsdata);

    echo " " . strtr( $jsdata->status , $ar_tr) . " ";
    print " <a href='set_finish.php?id=" . $row['id'] . "'>set_finish</a>";
    echo  " | ";
    print " <a href='set_unfinish.php?id=" . $row['id'] . "'>set_unfinish</a>";
    echo  " | ";
    print " <a href='set_doing.php?id=" . $row['id'] . "'>set_doing</a>";
    echo  " | ";
    print " <a href='set_bin.php?id=" . $row['id'] . "'>set_bin</a>";
    print ""  . "\n";
}

    ?>
    </div>


</body>
</html>